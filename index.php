<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Hub Undefined</title>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/animate.css">
        <link rel="stylesheet" href="assets/css/hover-min.css">
        <link rel="stylesheet" href="assets/css/theme.css">
        <script type="text/javascript" src="assets/js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="assets/js/angular.min.js"></script>
    </head>
    <body ng-app="undefinedApp">
        <div class="container-website">
          <div class="left-menu" ng-controller="LinkController">
              <div class="logo text-center">
                  <img src="assets/images/logo.png" alt="Undefined Hub">
              </div>
              <ul class="links">
                  <li ng-repeat="link in links">
                      <a class="li-link" target="{{link.internal ? '' : '_blank' }}" href="{{link.url}}">{{link.label}} <img class="icon" ng-show="link.icon" src="assets/icons/{{link.icon}}" alt=""></a>
                  </li>
              </ul>
          </div>
          <div class="project-container" ng-controller="ProjectController">
              <div class="col-sm-12 search-container">
                <div class="search-div">
                  <input type="text" ng-focus="onFocus($event)" ng-blur="onBlur($event)" ng-model="projectsSearch" placeholder="Rechercher" class="pull-right s-input">
                </div>
              </div>
              <div class="grid">
                <div class="col-lg-3 col-md-4 col-sm-4 isotope-item" ng-repeat="project in (filteredItems = (projects | filter:{name: projectsSearch})) | orderBy : 'name'" my-repeat-directive>
                    <div class="project-item">
                      <a href="{{project.url}}" target="_blank" class="ctn-img" ng-show="project.image">
                         <img ng-src="assets/projects/{{project.image || 'default.png'}}" />
                      </a>
                      <div class="informations" ng-class="project.links.length % 2 != 0 ? 'impair' : ''">
                          <a href="{{project.url}}" target="_blank" class="project-name">
                            {{project.name}}
                          </a>
                          <ul class="project-links" ng-class="project.links.length > 4 ? 'more-links' : ''">
                              <li ng-repeat="(key, plink) in project.links" ng-class="project.links.length  == (key + 1) ? 'long-link' : ''">
                                  <a class="li-link" href="{{plink.url}}" target="_blank">{{plink.label}}</a>
                              </li>
                              <li class="more" ng-click="expendLinks($event)" ng-show="project.links.length > 4"></li>
                          </ul>
                      </div>
                    </div>
                </div>
              </div>
          </div>
        </div>
        <script type="text/javascript" src="assets/js/imageloaded.min.js"></script>
        <script type="text/javascript" src="assets/js/isotope.min.js"></script>
        <script type="text/javascript" src="assets/js/app.js"></script>
    </body>
</html>
