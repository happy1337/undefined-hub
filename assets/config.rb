require 'compass/import-once/activate'

Encoding.default_external = "utf-8"

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "css"
sass_dir = "sass"
images_dir = "images"
fonts_dir = "fonts"
javascripts_dir = "js"

# Development OR Production
# environment = :development
environment = :production

output_style = (environment == :development) ? :expanded : :compressed;
sourcemap = true;

# To enable relative paths to assets via compass helper functions. Uncomment:
relative_assets = true

# AUTOPREFIXER
# gem install autoprefixer-rails
#require 'autoprefixer-rails'

#on_stylesheet_saved do |file|
#  css = File.read(file)
#  map = file + '.map'

#  if File.exists? map
#    result = AutoprefixerRails.process(css,
#      from: file,
#      to:   file,
#      map:  { prev: File.read(map), inline: false })
#    File.open(file, 'w') { |io| io << result.css }
#    File.open(map,  'w') { |io| io << result.map }
#  else
#    File.open(file, 'w') { |io| io << AutoprefixerRails.process(css) }
#  end
#end


# To disable debugging comments that display the original location of your selectors. Uncomment:
# line_comments = false
