var undefinedApp = angular.module('undefinedApp', []);

undefinedApp.directive('myRepeatDirective', function() {
  return function(scope, element, attrs) {
    if (scope.$last){
      scope.grid.imagesLoaded( function() {
        scope.grid.addClass('is-instance');
        scope.grid.isotope({
          itemSelector: '.isotope-item',
          percentPosition: true,
          masonry: {
            columnWidth: '.isotope-item'
          }
        });
      });
    }
  };
});

undefinedApp.controller('ProjectController', ['$scope','$http', function($scope, $http) {
    $http.get('assets/data/projects.json').success(function(data) {
	    $scope.projects = data.projects;
    });

    $scope.grid = angular.element('.grid');

    $scope.filteredItems = [];
    $scope.$watch('filteredItems', function(){
      $scope.grid.imagesLoaded( function() {
        if($scope.grid.hasClass('is-instance')){
          $scope.grid.isotope( 'reloadItems' ).isotope();
        }
      });
    }, true);

    $scope.expendLinks = function($event){
        angular.element($event.target).closest('.project-links').toggleClass('open');
        $scope.grid.isotope().isotope('layout');
    }

    $scope.onFocus = function($event){
        angular.element($event.target).closest('.search-div').addClass('focus');
    }

    $scope.onBlur = function($event){
        angular.element($event.target).closest('.search-div').removeClass('focus');
    }
}]);

undefinedApp.controller('LinkController', ['$scope','$http', function($scope, $http) {
    $http.get('assets/data/links.json').success(function(data) {
	    $scope.links = data.links;
    });
}]);
